# -*- coding: utf-8 -*-
from odoo import api, fields, models


class DeliveryArea(models.Model):
    _name = 'delivery.area'
    _description = 'Shipping Areas'

    xx_delivery_area = fields.One2many('choose.delivery.carrier', inverse_name='xx_delivery_area_id',
                                       string='Delivery Areas')
    xx_area_name = fields.Char(string='Area Name')
    xx_area_price = fields.Float(string='Area Price')

